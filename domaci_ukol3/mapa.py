# 20 bodu
'''Kdo hral textovou hru od Googlu tak bude v obraze. Cilem bude udelat mapu. Muzete si udelat vlastni nebo pouzit moji.
Mapa se bude skládat z několika částí a bude reprezentována textem. Obrázek mapy se nachází pod tímto textem
Cilem toho cviceni bude spise vymyslet jak toto cviceni vyresit. Samotna implementace nebude tak narocna.

Prace s dvourozmernym polem je seznamem (polem) je jednoducha

pole = [['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l']]

Dvourozmerne pole obsahuje radky a sloupce. Kazda hodnota ma svuj jedinecny index definovany indexem radku a sloupce
napr. pole[0][1] vybere hodnotu na prvni radku ve druhem sloupci 'b'.
      pole[1][3] vybere hodnotu na druhem radku ve ctvrtem sloupci 'h'.
Vice informaci najdete na internetu: https://www.tutorialspoint.com/python/python_2darray.htm

Mapa (dvourozmerne pole) predstavuje bludiste. Vase postava se na zacatku hry bude nachazet na pozici pismene S (index [7][0]).
Cílem je dostat se do cile, ktery oznacuje pismeno F (index [0][9]).
Budete se ptat uzivatele stejne jako v textove hre na informaci o tom kterym smerem chce jit - nahoru, doprava, dolu, doleva.
Po každém zadání posunu vykreslíte mapu, kde bude zobrazena pozice postavicky v mape a pozici v jejim primem okoli. Bude to vypadat asi takto:

      ?????
      ?***?
      ?*P.?
      ?*.*?
      ?????

Tečky představují cestu (místo kam může postavička stoupnout). Hvězdičky představují zeď, tam postavička nemůže vstoupit.
Ostatní okolí postavy (zbytek mapy) bude ukryt postavě a budou ho představovat otazníky.

# BONUS: 5 bodů
# Udělejte verzi, kde se bude ukládat a při vykreslování mapy zobrazovat i místo, které postava už prošla a tudíž ho zná.
'''

game_map = [['*','*','*','*','*','*','.','.','.','F'],
            ['*','*','*','*','.','.','.','*','*','*'],
            ['.','.','.','.','.','*','*','*','*','*'],
            ['.','*','*','*','*','*','*','*','.','.'],
            ['.','.','.','*','*','.','.','.','.','.'],
            ['*','*','.','.','.','.','*','*','*','*'],
            ['*','*','.','*','*','*','*','*','*','*'],
            ['S','.','.','.','.','.','.','.','.','.']]

    hraci_pole = [['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['?', '?', '?', '?', '?', '?', '?', '?', '?', '?'],
                  ['S', '?', '?', '?', '?', '?', '?', '?', '?', '?']]

    row = 7
    col = 0

    out_of_array = 0
    wall = 0

    if row != 7:
        hraci_pole[row + 1][col] = game_map[row + 1][col]
    if row != 0:
        hraci_pole[row - 1][col] = game_map[row - 1][col]
    if col != 9:
        hraci_pole[row][col + 1] = game_map[row][col + 1]
    if col != 0:
        hraci_pole[row][col - 1] = game_map[row][col - 1]

    for lists in hraci_pole:
        print(" ".join(lists))

    while (row != 0) or (col != 9):
        str_position = input("Chces hrat nahoru / dolu / vlevo / vpravo? ")
        print ("")

        if str_position == "nahoru":
            new_row = row - 1
            new_col = col
            direction = "✓"

        elif str_position == "dolu":
            new_row = row + 1
            new_col = col
            direction = "✓"

        elif str_position == "vlevo":
            new_row = row
            new_col = col - 1
            direction = "✓"

        elif str_position == "vpravo":
            new_row = row
            new_col = col + 1
            direction = "✓"

        else:
            print("Nerozumím, zadej kam chceš jet ještě jednou")
            continue

        if new_row >= len(game_map) or new_row < 0 or new_col >= len(game_map[0]) or new_col < 0:
            out_of_array += 1
            print("Jsi mimo hrací pole")

        elif game_map[new_row][new_col] == "*":
            wall += 1
            print("Narazil/a jsi do zdi")
        else:
            hraci_pole[new_row][new_col] = "P"

            if new_row != len(game_map) - 1:
                hraci_pole[new_row + 1][new_col] = game_map[new_row + 1][new_col]
            if new_row != 0:
                hraci_pole[new_row - 1][new_col] = game_map[new_row - 1][new_col]
            if new_col != len(game_map[0]) - 1:
                hraci_pole[new_row][new_col + 1] = game_map[new_row][new_col + 1]
            if new_col != 0:
                hraci_pole[new_row][new_col - 1] = game_map[new_row][new_col - 1]

            hraci_pole[row][col] = direction
            game_map[row][col] = direction
            row = new_row
            col = new_col

        for lists in hraci_pole:
            print(" ".join(lists))

    print("Yes! Vyhrál jsi!")

show_map()


