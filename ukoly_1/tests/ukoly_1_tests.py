import unittest
from ukoly_1.escape_sequences import escape_sequences
from ukoly_1.concatenation import concatenation


class TestExercise1(unittest.TestCase):
    def test_escapes(self):
        self.assertEqual(escape_sequences(), '\'Beautiful is better than ugly.\'\n\"Explicit is better than implicit.\"\n\\Simple is better than complex.\\\n\tComplex is better than complicated.')

    def test_concatenation(self):
        self.assertEqual(concatenation(), '{} Leagues Under the Sea'.format(20000))