"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.
"""



pocetpokusu = 0

print('UHÁDNEŠ ČÍSLO?')
print('Nejdřív napiš svoje jméno')
jmeno = input()

cislo = 6
temer = 5
temer2 = 7

print('Tak jo, ' + jmeno + ', myslím si číslo od 1 do 10. Uhodneš ho? Máš 4 pokusy.')


while pocetpokusu < 4:
    print('Hádej')
    guess = input()
    guess = int(guess)

    pocetpokusu = pocetpokusu + 1

    if guess == temer or (guess > cislo and guess == temer2):
        print('těsně vedle týjo')

    elif guess < temer:
        print('haha, moc nízký číslo')

    elif guess > temer2:
        print('haha, to je moc vysoký číslo')

    else:
        guess == cislo
        break

if guess == cislo:
    pocetpokusu = str(pocetpokusu)
    print('Dobře ' + jmeno + '! To je to číslo! Uhodl/a jsi na ' + pocetpokusu + ' pokus.')


if guess != cislo:
    cislo = str(cislo)
    print('Co se dá dělat tvé pokusy vypršely. Bylo to číslo ' + cislo)


print ('Chceš hrát ještě jednou? ')
odpoved = input()

if odpoved == 'ano' or odpoved == 'jo' or odpoved == 'Ano' or odpoved == 'Jo':
    print ('ok, tak si to stusť jestě jednou')
else:
    print ('ok, tak ne')


